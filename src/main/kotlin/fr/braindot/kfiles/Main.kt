package fr.braindot.kfiles

object Main {

    /**
     * Simple boilerplate function to test that everything is working correctly.
     */
    fun main() {
        println("Successfully loaded the library!")
    }

}