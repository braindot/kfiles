import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

ext["PUBLISH_GROUP_ID"] = "fr.braindot"
ext["PUBLISH_ARTIFACT_ID"] = "kfiles"
ext["PUBLISH_VERSION"] = "1.0"

plugins {
    java
    id("org.jetbrains.dokka") version "0.10.0"
    kotlin("jvm") version "1.3.60"
}

apply(from = "publish-maven-central.gradle")

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2")

    testImplementation("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

val dokka by tasks.getting(org.jetbrains.dokka.gradle.DokkaTask::class) {
    outputDirectory = "$buildDir/dokka"
    outputFormat = "html"

    configuration {
        includeNonPublic = false
        skipDeprecated = false
        reportUndocumented = true
        moduleName = rootProject.name

        sourceLink {
            path = "src/main/kotlin"
            url = "https://gitlab.com/braindot/kfiles/tree/master"
            lineSuffix = "#L"
        }

        jdkVersion = 8
    }
}